package session

import (
	"crypto/aes"
	"crypto/cipher"
	"crypto/rsa"
	"encoding/base64"
	"encoding/binary"
	"encoding/hex"
	"errors"
	"gitlab.com/bobburns/message"
	"log"
	"net"
	"os"
)

const noncestr = "757ba69fd154ee1ebdf54b3e"

var (
	ConnectErr   = errors.New("No Connection")
	NackErr      = errors.New("Nack Error")
	NameTrustErr = errors.New("Requestee doesn't trust you")
	IPTrustErr   = errors.New("This IP is not trusted")
	KeyFileErr   = errors.New("Bad Key File")
	NoKeyErr     = errors.New("No Key File")
	FormatErr    = errors.New("Bad Format")
	PassErr      = errors.New("Bad Password")
	NoFileErr    = errors.New("File Does not Exist")
	AuthErr      = errors.New("Authentication Error")
	SetupErr     = errors.New("Setup Error")
	ServerErr    = errors.New("Server Error")
	FamKeyErr    = errors.New("Not Fam. No Keys")
)
var (
	Ack  = []byte{0xa0, 0xa0, 0xa0, 0x00}
	Nack = []byte{0xa0, 0xa0, 0xa0, 0xff}
	//client to server calls
	//KeyRequest = []byte{0xa0, 0xa0, 0xa0, 0x01}
	SendMPre   = []byte{0xa0, 0xa0, 0xa0, 0x01}
	SendMess   = []byte{0xa0, 0xa0, 0xa0, 0x02}
	RetrMPre   = []byte{0xa0, 0xa0, 0xa0, 0x03}
	RetrMess   = []byte{0xa0, 0xa0, 0xa0, 0x04}
	ClientAuth = []byte{0xa0, 0xa0, 0xa0, 0x05}
	MessInit   = []byte{0xa0, 0xa0, 0xa0, 0x06}
	ReqMeta    = []byte{0xa0, 0xa0, 0xa0, 0x07}
	CHand      = []byte{0xa0, 0xa0, 0xa0, 0x08}
	DelMess    = []byte{0xa0, 0xa0, 0xa0, 0x09}
	CChPass    = []byte{0xa0, 0xa0, 0xa0, 0x0d}
	// server response opcodes
	//ServKey   = []byte{0xa0, 0xa0, 0xa0, 0x02}
	ServMess  = []byte{0xa0, 0xa0, 0xa0, 0x0a}
	ServAuth  = []byte{0xa0, 0xa0, 0xa0, 0x0b}
	ServMarsh = []byte{0xa0, 0xa0, 0xa0, 0x0c}
	//SHand     = []byte{0xa0, 0xa0, 0xa0, 0x0e}
	// Error Payload
	SendDenyName = []byte{0xa0, 0xa0, 0xa0, 0x20}
	SendDenyIP   = []byte{0xa0, 0xa0, 0xa0, 0x21}
	SendNoKey    = []byte{0xa0, 0xa0, 0xa0, 0x22}
	BadFormat    = []byte{0xa0, 0xa0, 0xa0, 0x23}
	NoFile       = []byte{0xa0, 0xa0, 0xa0, 0x24}
	SendDenyMess = []byte{0xa0, 0xa0, 0xa0, 0x25}
	SendDenyAuth = []byte{0xa0, 0xa0, 0xa0, 0x26}
	BadSetup     = []byte{0xa0, 0xa0, 0xa0, 0x27}
	BadEncrypt   = []byte{0xa0, 0xa0, 0xa0, 0x28}
	BadWrite     = []byte{0xa0, 0xa0, 0xa0, 0x29}
)

type Session struct {
	//tcp connection
	Conn net.Conn

	// clients account name
	Account string

	// to used to encrypt message to
	ToFrom string
	// opcode sent to server/client
	sendOp []byte
	// opcode received by server/client
	RecOp []byte

	// payload data sent to server/client
	payload []byte

	// payload length
	payLen int

	// data received by server/client
	recData []byte

	// length of data received
	dataLen int

	// private key of client
	privKey *rsa.PrivateKey

	// public key of server/client
	pubKey *rsa.PublicKey

	// server side mailbox
	Mail *message.MetaFiles

	// authorized flag
	auth bool

	// random session key
	sessionKey string
}

// setup logfile
func init() {
	path := ""
	_, err := os.Stat("/var/log/fam/")
	if os.IsNotExist(err) {
		log.Println("no log file setup in /var/log/fam/ using .famlog instead")
		path = ".famlog"
	} else {
		path = "/var/log/fam/fam.log"
	}
	f, err := os.OpenFile(path, os.O_WRONLY|os.O_CREATE|os.O_APPEND, 0644)
	if err != nil {
		log.Fatal(err)
	}

	//set ouput of logs to f
	log.SetOutput(f)
}

//
// Shared Functions
//

// init values for session
func NewSession(c net.Conn) *Session {
	return &Session{
		Conn:    c,
		sendOp:  make([]byte, 4),
		RecOp:   make([]byte, 4),
		payload: make([]byte, 2048),
		recData: make([]byte, 2048),
	}
}

// I'm not sure who calls this right now
func (s *Session) ConfigMail(path string) {
	s.Mail = message.NewMetaFiles(path)
}

// send opcode + payload to server or client
func (s *Session) send() error {
	outb := make([]byte, 4)
	binary.BigEndian.PutUint32(outb, uint32(s.payLen))

	_, err := s.Conn.Write(s.sendOp[0:4])
	//log.Println("write op")
	if err != nil {
		log.Println("Could not send payload + opcode")
		return err
	}
	_, err = s.Conn.Write(outb[0:4])
	if err != nil {
		log.Println("could not send len bytes")
		return err
	}
	if s.payLen > 0 {
		i := 0
		writen := 0
		for {
			n, err := s.Conn.Write(s.payload[i:s.payLen])
			if err != nil {
				log.Println("could not send payload")
				return err
			}
			writen += n
			if writen == s.payLen {
				break
			}
			i = n
		}
	}

	return nil
}

// receive opcode + data from server or client
func (s *Session) recv() error {
	// get response
	inb := make([]byte, 4)
	lenb := make([]byte, 4)

	_, err := s.Conn.Read(inb[0:4])
	if err != nil {
		log.Println("could not read opcode")
		return err
	}
	s.RecOp = inb[0:4]
	_, err = s.Conn.Read(lenb[0:4])
	if err != nil {
		log.Println("could not read len")
		return err
	}
	// convert byte array to int
	plen := int(binary.BigEndian.Uint32(lenb[0:4]))
	if plen > 2048 {
		return errors.New("payload to large")
	}
	//s.RecData = make([]byte, plen)
	if plen > 0 {
		i := 0
		readn := 0
		for {
			n, err := s.Conn.Read(s.recData[i:plen])
			if err != nil {
				log.Println("error with read bytes")
				return err
			}
			readn += n
			if readn == plen {
				break
			}
			i = n
		}
		s.dataLen = readn
	}

	return nil
}

// encrypt payload after authentication is complete
func (s *Session) EncryptPayload() error {

	key := []byte(s.sessionKey)
	plaintext := s.payload[0:s.payLen]
	block, err := aes.NewCipher(key)
	if err != nil {
		return err
	}

	nonce, _ := hex.DecodeString(noncestr)

	aesgcm, err := cipher.NewGCM(block)
	if err != nil {
		return err
	}

	ciphertext := aesgcm.Seal(nil, nonce, plaintext, nil)
	ciphersafe := removeBase64Padding(base64.URLEncoding.EncodeToString(ciphertext))
	if err != nil {
		log.Println("could not base64 encode cipher")
		return err
	}
	s.payLen = len([]byte(ciphersafe))
	copy(s.payload, []byte(ciphersafe))
	return nil
}

// decrypt recieved data with session key
func (s *Session) DecryptPayload() error {

	key := []byte(s.sessionKey)
	ciphertext, err := base64.URLEncoding.DecodeString(addBase64Padding(string(s.recData[:s.dataLen])))
	if err != nil {
		log.Println("could not base 64 encode cipher text")
		return err
	}

	block, err := aes.NewCipher(key)
	if err != nil {
		log.Println("bad key")
		return err
	}

	nonce, _ := hex.DecodeString(noncestr)

	aesgcm, err := cipher.NewGCM(block)
	if err != nil {
		return err
	}

	plaintext, err := aesgcm.Open(nil, nonce, ciphertext, nil)
	if err != nil {
		log.Println(err)
		return err
	}
	s.dataLen = len(plaintext)
	copy(s.recData, plaintext[0:len(plaintext)])
	return nil
}

// convienience functions
func (s *Session) SendNack(op []byte) {
	s.sendOp = Nack[0:4]
	s.payLen = 4
	copy(s.payload, op[0:4])
	s.send()
}
func (s *Session) SendAck() {
	s.sendOp = Ack[0:4]
	s.payLen = 0
	s.send()
}
