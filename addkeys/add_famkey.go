package main

// utility to add decryption keys for fam client

import (
	"crypto/sha1"
	"fmt"
	"os"
)

func main() {
	user, key1, key2 := "", "", ""
	fmt.Print("username: ")
	fmt.Scan(&user)
	fmt.Print("key: ")
	fmt.Scan(&key1)
	fmt.Print("key: ")
	fmt.Scan(&key2)

	skey1 := fmt.Sprintf("%x", sha1.Sum([]byte(key1)))
	skey2 := fmt.Sprintf("%x", sha1.Sum([]byte(key2)))
	keystr := user + ":" + skey1[:32] + ":" + skey2[:24]
	fmt.Println(keystr)

	//write to file
	f, err := os.OpenFile(".keys", os.O_APPEND|os.O_WRONLY|os.O_CREATE, 0600)
	if err != nil {
		panic(err)
	}
	defer f.Close()

	_, err = f.WriteString(keystr + "\n")
	if err != nil {
		panic(err)
	}

	fmt.Println("keys successfully added")

}
