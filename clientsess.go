package session

import (
	"bufio"
	"bytes"
	"crypto/aes"
	"crypto/cipher"
	"crypto/rand"
	"crypto/rsa"
	"crypto/sha1"
	"crypto/x509"
	"encoding/hex"
	"encoding/json"
	"encoding/pem"
	"fmt"
	"gitlab.com/bobburns/message"
	"io/ioutil"
	"log"
	"os"
	"reflect"
	"strings"
)

//
// Client Functions
//

// Client call to change password
func (s *Session) ClientChgPass(n string) error {
	s.sendOp = CChPass[0:4]
	s.payLen = len(n)
	copy(s.payload, []byte(n))
	err := s.EncryptPayload()
	if err != nil {
		return err
	}
	err = s.send()
	if err != nil {
		return err
	}
	// wait for response
	err = s.recv()
	if err != nil {
		return err
	}
	if !bytes.Equal(s.RecOp[0:4], Ack[0:4]) {
		return PassErr
	}

	return nil

}

// Client call to retrieve message
// takes filename as arguement
// must be logged in authorized
func (s *Session) MessageGet(n string) error {
	s.sendOp = RetrMess[0:4]
	s.payLen = len(n)
	copy(s.payload, []byte(n))
	err := s.EncryptPayload()
	if err != nil {
		return err
	}
	err = s.send()
	if err != nil {
		return err
	}
	// wait for response
	// response should contain encrypted message
	err = s.recv()
	if err != nil {
		return err
	}

	return nil

}

// Client call to delete message
// takes file name as arguement
// must be authorized
func (s *Session) MessageDel(n string) error {
	s.sendOp = DelMess[0:4]
	s.payLen = len(n)
	copy(s.payload, []byte(n))
	err := s.EncryptPayload()
	if err != nil {
		return err
	}
	err = s.send()
	if err != nil {
		return err
	}

	// server responds with ack if success
	err = s.recv()
	if err != nil {
		return err
	}
	if !bytes.Equal(s.RecOp[0:4], Ack[0:4]) {
		return ServerErr
	}

	return nil

}

// Encrypt Message Client side
// This gets double encrypted after session payload encrypt
// Client must have receivers keys to encrypt
func (s *Session) EncryptMessage(plaintext string) (string, error) {
	path := "addkeys/.keys"

	if _, err := os.Stat(path); os.IsNotExist(err) {
		log.Println("no keys file")
		return "", NameTrustErr
	}
	f, err := os.OpenFile(path, os.O_RDWR, 0600)
	if err != nil {
		log.Println("file open error")
		return "", err
	}
	defer f.Close()

	kstr, nstr := "", ""
	scanner := bufio.NewScanner(f)

	// find recievers keys
	for scanner.Scan() {
		line := scanner.Text()
		data := strings.Split(line, ":")
		if data[0] == s.ToFrom {
			//got fam
			kstr = data[1]
			nstr = data[2]
			break
		}
	}
	if kstr == "" {
		return "", FamKeyErr
	}
	key := []byte(kstr)

	block, err := aes.NewCipher(key)
	if err != nil {
		return "", err
	}

	nonce, _ := hex.DecodeString(nstr)

	aesgcm, err := cipher.NewGCM(block)
	if err != nil {
		return "", err
	}

	ciphertext := aesgcm.Seal(nil, nonce, []byte(plaintext), nil)
	cipherstr := fmt.Sprintf("%x", ciphertext)
	return cipherstr, nil
}

// Decrypt Message client side.
// All Messages to client should have been encrypted with clients aes keys
func (s *Session) DecryptMessage() (string, error) {
	err := s.DecryptPayload()
	if err != nil {
		return "", err
	}

	ciphertext, err := hex.DecodeString(string(s.recData[:s.dataLen]))
	if err != nil {
		return "", err
	}

	path := "addkeys/.mykey"

	if _, err := os.Stat(path); os.IsNotExist(err) {
		log.Println("no keys file")
		return "", NameTrustErr
	}
	f, err := os.Open(path)
	if err != nil {
		log.Println("file open error")
		return "", err
	}
	defer f.Close()

	kstr, nstr := "", ""
	scanner := bufio.NewScanner(f)

	scanner.Scan()
	line := scanner.Text()
	data := strings.Split(line, ":")
	kstr = data[0]
	nstr = data[1]
	if kstr == "" {
		return "", FamKeyErr
	}
	key := []byte(kstr)

	block, err := aes.NewCipher(key)
	if err != nil {
		return "", err
	}

	nonce, _ := hex.DecodeString(nstr)

	aesgcm, err := cipher.NewGCM(block)
	if err != nil {
		return "", err
	}

	plaintext, err := aesgcm.Open(nil, nonce, []byte(ciphertext), nil)
	if err != nil {
		log.Println(err)
		return "", err
	}
	return string(plaintext), nil
}

// Client side decrypt session key
func (s *Session) DecryptSessKey() (string, error) {
	// check for Private Key
	if reflect.ValueOf(s.privKey).IsNil() {
		return "", NoKeyErr
	}
	//read input text
	in := s.recData[0:s.dataLen]
	//decrypt
	out, err := rsa.DecryptOAEP(sha1.New(), rand.Reader, s.privKey, in, []byte("fam"))
	if err != nil {
		log.Printf("error decrypting: %s", err)
		return "", err
	}

	return string(out), nil
}

// Client requests to send message to server (set up)
// takes addressee as arguement
func (s *Session) MessagePreSend(to string) error {
	// send from:to
	s.payLen = len(to)
	copy(s.payload, []byte(to))
	err := s.EncryptPayload()
	if err != nil {
		return err
	}
	s.sendOp = SendMPre[0:4]

	if err = s.send(); err != nil {
		return err
	}
	err = s.recv()
	if err != nil {
		return err
	}
	if !bytes.Equal(s.RecOp[0:4], Ack[0:4]) {
		return NackErr
	}

	return nil

}

// Client sends encrypted message to server
func (s *Session) MessageSend(enc string) error {

	/*
		if reflect.ValueOf(s.PubKey).IsNil() {
			return NoKeyErr
		}
	*/

	ndata := []byte(enc)
	s.payLen = len(ndata)
	copy(s.payload, ndata[:len(ndata)])
	err := s.EncryptPayload()
	if err != nil {
		return err
	}
	s.sendOp = SendMess[0:4]

	if err := s.send(); err != nil {
		return err
	}

	err = s.recv()
	if err != nil {
		return err
	}

	return nil
}

// Client Requests to init mail struct on server
// must be called before getting mail meta data
func (s *Session) RequestInitMess() error {
	s.payLen = 0
	s.sendOp = MessInit[0:4]
	if err := s.send(); err != nil {
		return err
	}
	err := s.recv()
	if err != nil {
		return err
	}
	return nil
}

// Client requests Meta-Data for mailbox on Server
func (s *Session) RequestMeta() error {
	s.payLen = 0
	s.sendOp = ReqMeta[0:4]
	if err := s.send(); err != nil {
		return err
	}
	err := s.recv()
	if err != nil {
		return err
	}

	err = s.DecryptPayload()
	if err != nil {
		return err
	}

	indata := s.recData[:s.dataLen]
	m := message.MetaFiles{}
	err = json.Unmarshal([]byte(indata), &m)
	if err != nil {
		return err
	}
	s.Mail = &m
	return nil
}

// Client Authenticates with server.
// Authentication is cleartext over the wire
func (s *Session) ClientAuth(from, pass string) error {
	if s.Conn == nil {
		return ConnectErr
	}

	ndata := from + ":" + pass
	s.sendOp = ClientAuth[0:4]
	s.payLen = len(ndata)
	copy(s.payload, []byte(ndata))
	err := s.send()
	if err != nil {
		return err
	}
	err = s.recv()
	if err != nil {
		return err
	}
	if !bytes.Equal(s.RecOp[0:4], ServAuth[0:4]) {
		return AuthErr

	}
	s.auth = true
	s.Account = from
	err = s.GetPrivKey()
	if err != nil {
		return err
	}
	// get session key
	skey, err := s.DecryptSessKey()
	if err != nil {
		return err
	}
	s.sessionKey = skey
	return nil
}

// Function to get clients rsa private key
// Used to decrypt session key
func (s *Session) GetPrivKey() error {

	// get priv key

	pemData, err := ioutil.ReadFile(s.Account + "/keypriv.pem")
	if err != nil {
		log.Printf("bad key file: %s\n", err)
		return KeyFileErr
	}

	//extract pem encoded private key
	block, _ := pem.Decode(pemData)
	if block == nil {
		log.Println("bad key data: not PEM encoded")
		return KeyFileErr
	}
	if block.Type != "RSA PRIVATE KEY" {
		log.Printf("unknown key type: %s\n", block.Type)
		return KeyFileErr
	}

	// decode the rsa private key
	priv, err := x509.ParsePKCS1PrivateKey(block.Bytes)
	if err != nil {
		log.Printf("bad private key: %s\n", err)
		return KeyFileErr
	}
	s.privKey = priv
	return nil
}
