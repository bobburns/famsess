package session

import (
	"bufio"
	"crypto/rand"
	"crypto/rsa"
	"crypto/sha1"
	"crypto/x509"
	"encoding/json"
	"encoding/pem"
	"gitlab.com/bobburns/message"
	"io/ioutil"
	"log"
	"os"
	"reflect"
	"strings"
)

//
//  Server Functions
//

// Server parse incomming opcode from client
func (s *Session) ParseIncoming() error {
	if s.Conn == nil {
		return ConnectErr
	}
	err := s.recv()

	if err != nil {
		log.Println("could not read incomming")
		return err
	}
	return nil

}

// helper function to validate authticated connection
func (s *Session) validateAuth() bool {
	if s.auth {
		return true
	}
	s.SendNack(SendDenyAuth[0:4])
	return false
}

// Client sets up server to receive mail message
// Client sends who the message is for
func (s *Session) MessagePreRecv() error {

	if !s.validateAuth() {
		return AuthErr
	}
	//get name to handle messge for
	err := s.DecryptPayload()
	if err != nil {
		s.SendNack(BadEncrypt[:])
		return FormatErr
	}

	ndata := s.recData[0:s.dataLen]
	s.ToFrom = string(ndata)

	s.SendAck()
	return nil
}

// Server recieves encrypted message and saves to
// corresponding mail box
func (s *Session) MessageRecv() error {
	if !s.validateAuth() {
		return AuthErr
	}
	err := s.DecryptPayload()
	if err != nil {
		s.SendNack(BadEncrypt[:])
		return FormatErr
	}

	ndata := string(s.recData[:s.dataLen])
	path := "users/" + s.ToFrom + "/mail/metafile.json"
	// change to s.Mail
	meta := message.NewMetaFiles(path)
	meta.Create(ndata)
	meta.Update()

	s.SendAck()
	return nil

}

// Client requests to delete mail
// Server respondes with ack
func (s *Session) MessageServDel() error {
	if !s.validateAuth() {
		return AuthErr
	}
	err := s.DecryptPayload()
	if err != nil {
		s.SendNack(BadEncrypt[0:4])
		return FamKeyErr
	}

	ndata := string(s.recData[0:s.dataLen])
	ndata = strings.TrimSpace(ndata)

	if err != nil {
		s.SendNack(BadEncrypt[0:4])
		return err
	}
	path := "users/" + s.Account + "/mail/metafile.json"
	// change to s.Mail
	meta := message.NewMetaFiles(path)
	err = meta.Delete(string(ndata))
	if err != nil {
		s.SendNack(NoFile[0:4])
		return err
	}
	meta.Update()

	s.SendAck()
	return nil
}

// Client requests Server to initialize mail metadata
func (s *Session) MessageInit() error {
	if !s.validateAuth() {
		return AuthErr
	}
	path := "users/" + s.Account + "/mail/metafile.json"
	//TODO error check account
	s.Mail = message.NewMetaFiles(path)

	s.SendAck()
	return nil
}

// Client requests meta data from server
// Message Init must be called first
func (s *Session) ServMeta() error {
	if !s.validateAuth() {
		return AuthErr
	}
	if reflect.ValueOf(s.Mail).IsNil() {
		s.SendNack(BadSetup[0:4])
		return SetupErr
	}
	if reflect.ValueOf(s.pubKey).IsNil() {
		s.SendNack(BadSetup[0:4])
		return NoKeyErr
	}

	data, err := json.Marshal(s.Mail)
	if err != nil {
		s.SendNack(BadFormat[0:4])
		return err
	}
	s.sendOp = ServMarsh[0:4]
	s.payLen = len(data)
	copy(s.payload, data[0:len(data)])
	err = s.EncryptPayload()
	if err != nil {
		s.SendNack(BadEncrypt[:])
	}
	s.send()
	return nil
}

// Server serves message in response to Client request
// s.Mail must be populated
func (s *Session) MessageRetr() error {
	if !s.validateAuth() {
		return AuthErr
	}

	// get message data and send it
	err := s.DecryptPayload()
	if err != nil {
		s.SendNack(BadEncrypt[0:4])
		return FamKeyErr
	}

	mail := string(s.recData[0:s.dataLen])
	data, err := s.Mail.Read(mail)
	if err != nil {
		log.Println("error with messageretr()")
		s.SendNack(NoFile[0:4])
		return err
	}
	s.payLen = len(data)
	copy(s.payload, data[0:len(data)])
	err = s.EncryptPayload()
	if err != nil {
		s.SendNack(BadEncrypt[0:4])
		return err
	}
	s.sendOp = ServMess[0:4]
	s.send()
	return nil

}

// Server side authorize
func (s *Session) ServerAuth() error {

	// retrieve message for owner:number:pass

	indata := s.recData[0:s.dataLen]
	fnames := strings.Split(string(indata), ":")
	if fnames[0] == "" || fnames[1] == "" {
		s.SendNack(BadFormat[0:4])
		return FormatErr
	}

	err := s.localauth(fnames[0], fnames[1])
	if err != nil {
		return err
	}

	path := "users/" + fnames[0] + "/keypub.pem"
	if _, err = os.Stat(path); os.IsNotExist(err) {
		log.Println("no pub key file for ", fnames[0])
		s.SendNack(SendNoKey[0:4])
		return NoKeyErr
	}
	f2, err := os.Open(path)
	if err != nil {
		s.SendNack(SendNoKey[0:4])
		log.Println("Can't open key file")
		return NoKeyErr
	}
	defer f2.Close()
	pemData, err := ioutil.ReadAll(f2)
	if err != nil {
		s.SendNack(SendNoKey[0:4])
		log.Println("Can't read key file")
		return NoKeyErr
	}
	//extract pem encoded private key
	block, _ := pem.Decode(pemData)

	// decode the rsa private key
	pub, err := x509.ParsePKIXPublicKey(block.Bytes)
	if err != nil {
		log.Printf("bad public key: %s", err)
		s.SendNack(SendNoKey[0:4])
		return NoKeyErr
	}
	s.pubKey = pub.(*rsa.PublicKey)

	skey := genRandStr()
	ekey, err := s.EncryptSessKey(skey)
	if err != nil {
		log.Println("cannot encrypt")
		s.SendNack(SendNoKey[0:4])
		return NoKeyErr
	}

	s.sessionKey = skey
	s.auth = true
	s.sendOp = ServAuth[0:4]
	s.payLen = len(ekey)
	copy(s.payload, ekey[0:len(ekey)])
	//s.EncryptPayload()
	s.send()

	return nil
}

// local auth for server routines
// setup client mailbox
func (s *Session) localauth(user, pass string) error {

	s.auth = false
	path := "users/" + user + "/.code"

	if _, err := os.Stat(path); os.IsNotExist(err) {
		log.Println("no code file")
		s.SendNack(SendDenyAuth[0:4])
		return PassErr
	}
	f, err := os.Open(path)
	if err != nil {
		log.Println("file open error")
		s.SendNack(SendDenyAuth[0:4])
		return err
	}
	defer f.Close()

	scanner := bufio.NewScanner(f)
	scanner.Scan()
	line := scanner.Text()

	if pass != line {
		s.SendNack(SendDenyAuth[0:4])
		return PassErr
	}
	// setup session with client info
	s.Account = user
	mpath := "users/" + user + "/mail/metafile.json"
	s.Mail = message.NewMetaFiles(mpath)

	return nil
}

// get client public key to encrypt session key
func (s *Session) getClientPub() error {
	if s.Account == "" {
		return AuthErr
	}

	path := "users/" + s.Account + "/keypub.pem"
	pemData, err := ioutil.ReadFile(path)
	if err != nil {
		log.Printf("bad key file: %s", err)
		return err
	}

	//extract pem encoded private key
	block, _ := pem.Decode(pemData)
	if block == nil {
		log.Printf("bad key data: not PEM encoded")
		return err
	}
	if block.Type != "PUBLIC KEY" {
		log.Printf("unknown key type: %s", block.Type)
		return err
	}

	// decode the rsa private key
	pub, err := x509.ParsePKIXPublicKey(block.Bytes)
	if err != nil {
		log.Printf("bad public key: %s", err)
		return err
	}

	s.pubKey = pub.(*rsa.PublicKey)
	return nil
}

// Server change password from client request
func (s *Session) ServerChgPass() error {
	if !s.validateAuth() {
		return AuthErr
	}
	err := s.DecryptPayload()
	if err != nil {
		s.SendNack(BadEncrypt[:])
		return FormatErr
	}

	ndata := strings.Split(string(s.recData[:s.dataLen]), ":")
	if len(ndata) != 2 {
		s.SendNack(BadFormat[0:4])
		return FormatErr
	}
	path := "users/" + s.Account + "/.code"

	if _, err := os.Stat(path); os.IsNotExist(err) {
		log.Println("no code file")
		s.SendNack(SendDenyAuth[0:4])
		return PassErr
	}
	f, err := os.OpenFile(path, os.O_RDWR, 0600)
	if err != nil {
		log.Println("file open error")
		s.SendNack(SendDenyAuth[0:4])
		return err
	}
	defer f.Close()

	scanner := bufio.NewScanner(f)
	scanner.Scan()
	line := scanner.Text()

	if ndata[0] != line {
		s.SendNack(SendDenyAuth[0:4])
		return PassErr
	}

	_, err = f.WriteAt([]byte(ndata[1]), 0)
	if err != nil {
		s.SendNack(BadWrite[0:4])
		return ServerErr
	}
	f.Sync()

	s.SendAck()
	return nil
}

// Encrypt session key
func (s *Session) EncryptSessKey(m string) ([]byte, error) {

	//log.Println("got encrypt message")
	if reflect.ValueOf(s.pubKey).IsNil() {
		return []byte{}, NoKeyErr
	}
	// rsa can't be larger than 2048 bits
	if len(m) > 256 {
		m = m[0:255]
	}
	out, err := rsa.EncryptOAEP(sha1.New(), rand.Reader, s.pubKey, []byte(m), []byte("fam"))
	if err != nil {
		return []byte{}, err
	}

	return out, nil
}
