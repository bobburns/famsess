package session

import (
	"net"
	"testing"
)

func TestAliceLogin(t *testing.T) {

	//get tcp connection
	tcpAddr, err := net.ResolveTCPAddr("tcp4", "localhost:6532")
	if err != nil {
		t.Fatal(err)
	}

	conn, err := net.DialTCP("tcp", nil, tcpAddr)
	if err != nil {
		t.Fatal(err)
	}
	defer conn.Close()
	t.Log("got connection")

	s := NewSession(conn)
	t.Log("got session")

	err = s.ClientAuth("alice", "ccadd99b16cd3d200c22d6db45d8b6630ef3d936767127347ec8a76ab992c2ea")

	t.Log("got login")
	_ = "breakpoint"

	err = s.RequestInitMess()
	if err != nil {
		t.Fatal(err)
	}
	t.Log("got init mess")
	err = s.RequestMeta()
	if err != nil {
		t.Fatal(err)
	}
	t.Log("got RequestMeta()")
	mail := "howdy there. I'm getting put into little bits"
	s.ToFrom = "bob"
	encMess, err := s.EncryptMessage(mail)
	if err != nil {
		t.Fatal(err)
	}

	err = s.MessagePreSend("bob")
	if err != nil {
		t.Fatal(err)
	}
	t.Log("got MessagePreSend()")
	_ = "breakpoint"

	err = s.MessageSend(encMess)
	if err != nil {
		t.Fatal(err)
	}
	t.Log("got MessageSend()")

}
