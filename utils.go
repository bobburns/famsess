package session

import (
	mran "math/rand"
	"strings"
)

// utility functions
func addBase64Padding(value string) string {
	m := len(value) % 4
	if m != 0 {
		value += strings.Repeat("=", 4-m)
	}
	return value
}

func removeBase64Padding(value string) string {
	return strings.Replace(value, "=", "", -1)
}

func genRandStr() string {

	// generate random string
	str := make([]byte, 16)
	const letters = "ABCDEFGHIJKLMNOPQRSTUVWXYZabcdefghijklmnopqrstuvwxyz"
	for i := range str {
		str[i] = letters[mran.Intn(len(letters))] //???

	}
	return string(str)
}
